import java.sql.SQLException;

public class Application {
    public static void main(String[] args) throws SQLException {

        Database foo = Database.getInstance();
        foo.query("SELECT * FROM students WHERE id = 1");
        Database bar = Database.getInstance();
        bar.query("SELECT * FROM students WHERE id = 1");
        // The variable `bar` will contain the same object as
        // the variable `foo`.
    }
}