import java.sql.*;

public class Database {
        Connection con = null;
        ResultSet rs = null;
        Statement stmt = null;


        private static Database INSTANCE;

        private Database() {
                try {
                        Class.forName("org.postgresql.Driver");
                        con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/firstdb", "postgres", "Seven");
                        stmt = con.createStatement();
                        String sql = "CREATE TABLE students " + "(id INT PRIMARY KEY," +
                                " name           VARCHAR(15)," +
                                " surname        VARCHAR(15)," +
                                "grade           INT)";
                        stmt.executeUpdate(sql);
                        sql = "INSERT INTO students (id, name, surname, grade) "
                                + "VALUES (1, Jhon, Doe, 90);";
                        stmt.executeUpdate(sql);

                } catch (Exception e) {
                        e.printStackTrace();
                        System.err.println(e.getClass().getName() + ": " + e.getMessage());
                        System.exit(0);
                }
        }

        public static Database getInstance() {

                if (INSTANCE == null) {
                        INSTANCE = new Database();
                        return INSTANCE;
                }
                return INSTANCE;
        }

        public void query(String sql) throws SQLException{

                rs = stmt.executeQuery(sql);

                while(rs.next()){
                        System.out.println(rs.getInt("id") + " " + rs.getString("name") +
                                " " + rs.getString("surname") + " " + rs.getInt("grade"));
                }
        }


}

